﻿using ClothBazar.Entities;
using System;
using System.Data.Entity;

namespace ClothBazar.Database
{
    public class CBContext : DbContext
    {

        public CBContext() : base("ClothBazarConnection")
        {
            
        }

        public DbSet<Category> Categories{ get; set; }
        public DbSet<Product> Products { get; set; }
    }
}  
