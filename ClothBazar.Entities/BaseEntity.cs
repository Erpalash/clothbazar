﻿using System.ComponentModel.DataAnnotations;

namespace ClothBazar.Entities
{
    public class BaseEntity
    {
        [Key]
        public int ID{ get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
