﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClothBazar.Entities
{
    public class Category : BaseEntity
    {
        [Key]
        public List<Product> Products { get; set; }
    }
}
