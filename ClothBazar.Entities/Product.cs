﻿using System.ComponentModel.DataAnnotations;

namespace ClothBazar.Entities
{
    public class Product : BaseEntity
    {
        
        public decimal Price { get; set; }
        public Category Category{ get; set; }

    }
}
